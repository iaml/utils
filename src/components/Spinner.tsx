import React from 'react';
import styled, { keyframes } from 'styled-components';
import { cover } from 'polished';

const firstRingAnimation = keyframes`
0% {
    transform: rotateZ(20deg) rotateY(0deg);
}
100% {
    transform: rotateZ(100deg) rotateY(360deg);
}
`;
const secondRingAnimation = keyframes`
0% {
    transform: rotateZ(100deg) rotateX(0deg);
}
100% {
    transform: rotateZ(0deg) rotateX(360deg);
}
`;
const thirdRingAnimation = keyframes`
0% {
    transform: rotateZ(100deg) rotateX(-360deg);
}
100% {
    transform: rotateZ(-360deg) rotateX(360deg);
}
`;

const Container = styled.div`
    ${cover()}
    display: flex;
    align-items: center;
    justify-content: center;
`;

const SpinnerContainer = styled.div.attrs({
    children: (
        <>
            <div />
            <div />
            <div />
        </>
    ),
})`
    & * {
        box-sizing: border-box;
        position: absolute;
        display: block;
        border-radius: 50%;
        border: 3px solid #ff1d5e;
        opacity: 1;
    }
    height: 66px;
    width: 66px;
    padding: 3px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    overflow: hidden;
    box-sizing: border-box;

    & div:nth-child(1) {
        height: 60px;
        width: 60px;
        animation: ${firstRingAnimation} 1.5s infinite linear;
        border-width: 3px;
    }
    & div:nth-child(2) {
        height: calc(60px * 0.65);
        width: calc(60px * 0.65);
        animation: ${secondRingAnimation} 1.5s infinite linear;
        border-width: 2px;
    }
    & div:nth-child(3) {
        height: calc(60px * 0.1);
        width: calc(60px * 0.1);
        animation: ${thirdRingAnimation} 1.5s infinite linear;
        border-width: 1px;
    }
`;

export const Spinner = () => {
    return (
        <Container>
            <SpinnerContainer />
        </Container>
    );
};
