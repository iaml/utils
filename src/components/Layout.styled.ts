import styled from 'styled-components';

export const Layout = styled.main`
    display: flex;
    & > * {
        flex: 10;
    }
`;

export const Sidebar = styled.aside`
    display: flex;
    flex-direction: column;
    flex: 1;
    min-width: 24ch;
    a {
        color: whitesmoke;
        padding: 2ch;
        text-align: center;
        &:hover {
            background-color: grey;
        }
    }
`;

export const Content = styled.section`
    padding: 2ch;
`;