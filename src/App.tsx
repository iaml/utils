import React, { Suspense } from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import { BracketTheme } from './feature/BracketTheme';
import { Layout, Sidebar, Content } from './components/Layout.styled';
import { Playground } from './feature/Playground';
import { TransitionState } from './feature/TransitionState';
import { Spinner } from './components/Spinner';
import { BASE_ROUTE, IS_DEVELOPMENT } from './const';
import { GlobalStyle } from './style/global';

const { links, routes } = [BracketTheme, Playground, TransitionState]
    .filter(item => (IS_DEVELOPMENT ? true : !item.WIP))
    .reduce(
        (acc: { links: JSX.Element[]; routes: JSX.Element[] }, { Link, Route }) => {
            acc.links.push(Link);
            acc.routes.push(Route);
            return acc;
        },
        { links: [], routes: [] },
    );

const App: React.FC = () => {
    return (
        <Layout>
            <BrowserRouter basename={BASE_ROUTE}>
                <Sidebar>{links}</Sidebar>
                <Content>
                    <Suspense fallback={<Spinner />}>
                        <Switch>{routes}</Switch>
                    </Suspense>
                </Content>
            </BrowserRouter>
            <GlobalStyle />
        </Layout>
    );
};

export default App;
