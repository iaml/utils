import React, { lazy } from 'react';
import { Route, Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

export interface IClassRoute {
    Link: JSX.Element;
    Route: JSX.Element;
    WIP?: boolean;
}

export const createRoute = ({
    name,
    title,
    WIP = false,
}: {
    name: string;
    title: string;
    WIP?: boolean;
}): IClassRoute => {
    const Component = lazy(() => import(`../feature/${name}/${name}.tsx`));
    function EnhancedComponent<P>(props: P) {
        return (
            <>
                <Helmet>
                    <title>{title}</title>
                </Helmet>
                <Component {...props} />
            </>
        );
    }
    return {
        Link: (
            <Link to={name} key={name}>
                {title}
            </Link>
        ),
        Route: <Route path={`/${name}`} component={EnhancedComponent} key={name} />,
        WIP,
    };
};
