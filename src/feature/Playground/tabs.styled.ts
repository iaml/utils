import styled, { css } from "styled-components";

interface IndicatorProps {
    translate: number;
    scale: number;
}
const indicatorStyle = css<IndicatorProps>`
&:after {
    position: absolute;
    border-bottom: 2px solid purple;
    content: '';
    height: 0;
    bottom: 0;
    left: 0;
    right: 0;
    transform-origin: left center;
    transition: transform 200ms ease-out;
    transform: translateX(${({ translate }) => translate}px) scaleX(${({ scale }) => scale});
}
`;

export const Tabs = styled.ul<IndicatorProps>`
display: inline-flex;
align-items: center;
justify-content: flex-start;
list-style-type: none;
margin: 0;
margin-bottom: 3rem;
position: relative;
${indicatorStyle}
`;

export const TabsWrapper = styled.nav`
position: relative;
overflow-x: scroll;
scrollbar-width: none;
&::-webkit-scrollbar {
    display: none;
}
`;