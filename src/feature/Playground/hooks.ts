import React from 'react';

const getWidth = (ref: React.MutableRefObject<HTMLElement | null>) => {
    if (!ref || !ref.current) return 0;
    return ref.current.getClientRects()[0].width;
};
const TAB_MARGIN = 40;

export const useIndicator = (tabs: Array<{ ref: React.MutableRefObject<HTMLElement> }>, currentTabIndex: number) => {
    const tabsRef = React.useRef<HTMLElement>(null);

    const [{ scale, translate }, setIndicatorParams] = React.useState({
        scale: 0,
        translate: 0,
    });

    // on each route change, recalculate indicator offset and scale
    React.useEffect(() => {
        const tabWidths = tabs.map(({ ref }) => getWidth(ref));

        const { offset, width } = tabWidths.reduce(
            (acc, tabWidth, index) => {
                // for each tab before current, add its width and margin to offset
                if (index < currentTabIndex) {
                    acc.offset += tabWidth + TAB_MARGIN;
                }
                // for current tab, set indicator width equal to tab width
                if (index === currentTabIndex) acc.width = tabWidth;
                return acc;
            },
            { offset: 0, width: 0 },
        );
        // indicator scale is proportion to whole container width
        setIndicatorParams({
            scale: width / getWidth(tabsRef),
            translate: offset,
        });
    }, [tabs, currentTabIndex]);
    return {
        scale,
        translate,
        ref: tabsRef,
    };
};
