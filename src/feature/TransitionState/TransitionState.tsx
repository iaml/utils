import React, { useState } from 'react';
import styled from 'styled-components';

const Field = styled.div`
    display: flex;
    width: 50vw;
    height: 50vw;
    flex-wrap: wrap;
    margin: 5ch auto;
`;
const Item = styled.div`
    flex: 1 0 17%;
    background-color: white;
`;

const items = new Array(25).fill(null);

// const withTransition = (config: any) => (Component: React.Component<{}>) => {
//     return (props) => <Component {...props} />;
// };


const TransitionState: React.FC = () => {
    const [isShowing, setShowing] = useState(true);
    return (
        <>
            <label>
                <input type="checkbox" checked={isShowing} onChange={() => setShowing(!isShowing)} /> Mount items
            </label>
            <Field>
                {items.map((_, i) => (
                    <Item key={i} />
                ))}
            </Field>
        </>
    );
};

export default TransitionState;
