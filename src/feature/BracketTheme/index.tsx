import { createRoute } from '../../utils/ClassRoute';

export const BracketTheme = createRoute({ name: 'BracketTheme', title: 'Bracket Theme Generator' });
