import React, { useState, useMemo, useEffect, useCallback } from "react";
import styled, { css } from "styled-components";
import {
    interpolateRainbow,
    interpolateTurbo,
    interpolateSinebow,
    interpolatePlasma,
    interpolateSpectral,
} from "d3-scale-chromatic";
import { cover } from "polished";

const Item = styled.div`
    flex: 1;
    height: 42px;
`;

const FlexBox = styled.div<{ isCopied?: boolean }>`
    display: flex;
    position: relative;
    &:after {
        content: "Copy to clipboard";
        ${cover()}
        background: rgba(0,0,0,0.35);
        opacity: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        transition: opacity 100ms ease-in-out;
    }
    &:hover:after {
        opacity: 1;
    }
    ${(p) =>
        p.isCopied &&
        css`
            &:after {
                content: "Success!";
            }
        `}
`;

const Controls = styled.section`
    display: flex;
    flex-direction: column;
`;

const createSteps = (steps: number) =>
    Array(steps)
        .fill(0)
        .map((_, index): number => (index + 1) / steps);

const scales = {
    interpolateRainbow,
    interpolateTurbo,
    interpolateSinebow,
    interpolatePlasma,
    interpolateSpectral,
};

const Container = styled.article`
    margin-block-end: 1.5ch;
    header {
        font-weight: 500;
        font-size: 1.2rem;
    }
    & > * {
        margin-block-end: 1.2ch;
    }
`;

const Theme = ({
    interpolateFn,
    name,
}: {
    interpolateFn: (t: number) => string;
    name: string;
}) => {
    const [steps, setSteps] = useState(() => 8);
    const [customSteps, setCustomSteps] = useState(() => createSteps(8));
    useEffect(() => {
        setCustomSteps(createSteps(steps));
    }, [steps]);
    const [isInverse, setIsInverse] = useState(false);
    const [isCustom, setIsCustom] = useState(false);
    const [isCopied, setIsCopied] = useState(false);
    useEffect(() => {
        if (isCopied) setTimeout(() => setIsCopied(false), 3000);
    }, [isCopied]);
    const theme = useMemo(() => {
        const quants = isCustom ? customSteps : createSteps(steps);
        return (isInverse ? quants.reverse() : quants).map(interpolateFn);
    }, [steps, isInverse, isCustom, interpolateFn, customSteps]);
    return (
        <Container>
            <header>"d3.{name}"</header>
            <Controls>
                <label>
                    Number of steps{" "}
                    <input
                        type="number"
                        value={steps}
                        onChange={({ currentTarget: { value } }) =>
                            setSteps(Number(value))
                        }
                    />
                </label>
                <label>
                    Inverse{" "}
                    <input
                        type="checkbox"
                        checked={isInverse}
                        onChange={({ currentTarget: { checked } }) =>
                            setIsInverse(checked)
                        }
                    />
                </label>
                <label>
                    Custom{" "}
                    <input
                        type="checkbox"
                        checked={isCustom}
                        onChange={({ currentTarget: { checked } }) =>
                            setIsCustom(checked)
                        }
                    />
                </label>
                {isCustom &&
                    customSteps.map((step, index) => (
                        <input
                            key={ index}
                            max={1}
                            min={0}
                            step={ 0.01}
                            type="range"
                            value={step}
                            onChange={({ currentTarget: { value } }) => {
                                const newSteps = [...customSteps];
                                newSteps[index] = Number(value);
                                setCustomSteps(newSteps);
                            }}
                        />
                    ))}
            </Controls>
            <FlexBox
                isCopied={isCopied}
                onClick={useCallback(() => {
                    navigator.clipboard.writeText(JSON.stringify(theme));
                    setIsCopied(true);
                }, [theme])}
            >
                {theme.map((value, index) => (
                    <Item key={index} style={{ backgroundColor: value }} />
                ))}
            </FlexBox>
        </Container>
    );
};

const Colors = () => {
    return Object.entries(scales).map(([name, fn]) => (
        <Theme name={name} interpolateFn={fn} />
    ));
};
export default Colors;
